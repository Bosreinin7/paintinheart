//
//  ViewController.swift
//  PaintHeart
//
//  Created by Srei Nin on 4/25/19.
//  Copyright © 2019 kosign. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    var signaturView:PJRSignatureView = PJRSignatureView()
    
    @IBOutlet weak var imgview: UIImageView!
    var imgdelegate:ImageDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    @IBAction func pressClearBtn(_ sender: Any) {
        self.signaturView.clearSignature()
    }
    
    @IBAction func getSignImgBtn(_ sender: Any) {
      self.imgview.image = self.signaturView.getSignatureImage()
    }
    
    @IBAction func pressPopUpDraw(_ sender: Any) {
        let popup:ImgViewViewController = ImgViewViewController(nibName:"ImgViewViewController",bundle:Bundle.main)
         popup.drawImgDelegate = self

        UIApplication.shared.keyWindow?.rootViewController?.presentPopupViewController(popup, animationType: MJPopupViewAnimationFade, contentInteraction: MJPopupViewContentInteractionNone, setAlpha: 0.7)
        
    }
}

extension ViewController:ImageDelegate {
    func didFinishDrawContributors(img: UIImage) {
        self.imgview.image = img
    }
}
