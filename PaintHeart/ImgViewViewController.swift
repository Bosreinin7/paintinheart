//
//  ImgViewViewController.swift
//  PaintHeart
//
//  Created by Srei Nin on 4/25/19.
//  Copyright © 2019 kosign. All rights reserved.
//

import UIKit


protocol ImageDelegate: class {
    
    /// Called when **Save Button** in **GroupViewController** is tapped.
    ///
    /// - Parameters:
    ///   - shrn: Array of Groups that user selected.
    func didFinishDrawContributors(img:UIImage)
}
class ImgViewViewController: UIViewController {

    var imgdraw:PJRSignatureView = PJRSignatureView()
    
    @IBOutlet weak var timeLabel: UILabel!
    
    
    public var drawImgDelegate:ImageDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
        let dateFormatter = DateFormatter()
      
        dateFormatter.timeStyle = .medium
        
        let timeString = "\(dateFormatter.string(from: NSDate() as Date))"
        self.timeLabel.text = timeString
      self.imgdraw = PJRSignatureView(frame: CGRect(x: 0, y: 35, width: self.view.bounds.width, height: self.view.bounds.height - 150))
        self.view.addSubview(imgdraw)
       
        
    }

    @IBAction func save(_ sender: Any) {
        if self.imgdraw.getSignatureImage() != nil {
              self.drawImgDelegate?.didFinishDrawContributors(img: self.imgdraw.getSignatureImage())
        }
      
         self.dismissPopupViewController(self, animationType: MJPopupViewAnimationFade)
       
    }
  
    @IBAction func cencelDidTap(_ sender: Any) {
        self.imgdraw.clearSignature()
    }
}
